package com.zifeng.android_des;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.zifeng.androids.R;

public class DESActivity extends Activity implements OnClickListener {

	private EditText contentEdit;
	private TextView jiamiTv;
	private TextView jiemiTv;
	private String trimContent;
	private String result1;
	private String result2;
	private String key;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initView();
		key = "12345678";
	}

	private void initView() {
		contentEdit = (EditText) findViewById(R.id.content_edit);
		Button jiamiBtn = (Button) findViewById(R.id.jiami_bt);
		Button jiemiBtn = (Button) findViewById(R.id.jiemi_btn);
		jiamiTv = (TextView) findViewById(R.id.jiami_tv);
		jiemiTv = (TextView) findViewById(R.id.jiemi_tv);

		jiamiBtn.setOnClickListener(this);
		jiemiBtn.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.jiami_bt:
			trimContent = contentEdit.getText().toString().trim();
			result1 = null;
			try {
				result1 = DES.encryptDES(trimContent, key);
			} catch (Exception e) {
				e.printStackTrace();
			}
			jiamiTv.setText(result1);
			break;

		case R.id.jiemi_btn:
			result2 = null;
			try {
				Log.i("TAG", "result1:" + result1);
				result2 = DES.decryptDES(result1, key);
				Log.i("TAG", "result2:" + result2);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			jiemiTv.setText(result2);
			break;

		default:
			break;
		}
	}
}
