package com.zifeng.androids;

import com.zifeng.android_des.DESActivity;
import com.zifeng.android_md5.MD5Activity;
import com.zifeng.android_rsa.RSAActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MenuActivity extends Activity implements OnClickListener {

	private Button desBtn;
	private Button rsaBtn;
	private Button md5Btn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu_activity);
		initView();
	}

	private void initView() {
		desBtn = (Button) findViewById(R.id.des_btn);
		rsaBtn = (Button) findViewById(R.id.rsa_btn);
		md5Btn = (Button) findViewById(R.id.md5_btn);

		desBtn.setOnClickListener(this);
		rsaBtn.setOnClickListener(this);
		md5Btn.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.des_btn:
			startActivity(new Intent(this, DESActivity.class));
			break;

		case R.id.rsa_btn:
			startActivity(new Intent(this, RSAActivity.class));
			break;

		case R.id.md5_btn:
			startActivity(new Intent(this, MD5Activity.class));
			break;

		default:
			break;
		}
	}
}
