package com.zifeng.android_md5;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.zifeng.androids.R;

public class MD5Activity extends Activity {

	private EditText md5Edit;
	private Button jiamiBt;
	private TextView jiami_Tvs;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.md5);
		init();
	}

	private void init() {
		md5Edit = (EditText) findViewById(R.id.md5_edit);
		jiamiBt = (Button) findViewById(R.id.jiami_bt);
		jiami_Tvs = (TextView) findViewById(R.id.jiami_tvs);
		
		jiamiBt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String string = md5Edit.getText().toString();
				String md5Str = MD5.MD5Str(string);
				jiami_Tvs.setText(md5Str);
			}
		});
	}

}
